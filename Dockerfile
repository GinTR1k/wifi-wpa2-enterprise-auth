FROM freeradius/freeradius-server:latest

COPY raddb/ /etc/raddb/

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["radiusd", "-X"]
